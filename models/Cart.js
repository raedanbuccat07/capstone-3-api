const mongoose = require('mongoose')

const cartSchema = new mongoose.Schema({
    productId: {
        type: String,
        required: [true, 'Product Id is required']
    },
    price: {
        type: Number,
        required: [true, 'Price is required']
    },
    quantity: {
        type: Number,
        default: 1
    },
    ownerId: {
        type: String,
        required: [true, 'Owner Id is required']
    },
    sellerId: {
        type: String,
        required: [true, 'Seller Id is required']
    },
    productPicture: {
        type: String,
        require: [true, 'Product Picture is required']
    },
    productName: {
        type: String,
        require: [true, 'Product Name is required']
    },
})

module.exports = mongoose.model('Cart', cartSchema)