const mongoose = require('mongoose')

const productSchema = new mongoose.Schema({

    sellerId: {
        type: String,
        required: [true, "Seller ID is required"]
    },
    name: {
        type: String,
        required: [true, "Product name is required"]
    },
    description: {
        type: String,
        required: [true, "Description is required"]
    },
    price: {
        type: Number,
        required: [true, "Price is required"]
    },
    isActive:{
        type: Boolean,
        default: true
    },
    isFeatured: {
        type: Boolean,
        default: false
    },
    productPicture: {
        type: String,
        default: ""
    },
    productPictureId: {
        type: String,
        default: ""
    },
    profilePicture: {
        type: String,
        default: ""
    },
    profilePictureId: {
        type: String,
        default: ""
    },
    favorites: [
        {
            userId: {
                type: String,
                default: ""
            }
        }
    ],
    createdOn: {
        type: Date,
        default: new Date()
    }

})

module.exports = mongoose.model('Product', productSchema)