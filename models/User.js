const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({

    email: {
        type: String,
        required: [true, "Email is required"]
    },
    password: {
        type: String,
        required: [true, "Password is required"]
    },
    nickname: {
        type: String,
        required: [true, "Nickname is required"]
    },
    gender: {
        type: String,
        required: [true, "Gender is required"]
    },
    profilePicture: {
        type: String,
        default: ""
    },
    profilePictureId: {
        type: String,
        default: ""
    },
    isAdmin: {
        type: Boolean,
        default: false
    }

})

module.exports = mongoose.model('User', userSchema)