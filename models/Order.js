const mongoose = require('mongoose')

const orderSchema = new mongoose.Schema({

    userId: {
        type: String,
        required: [true, 'User Id is required']
    },
    totalAmount: {
        type: Number,
        required: [true, 'Total is required']
    },
    purchasedOn: {
        type: Date,
        default: new Date()
    },
    productsPurchased: [ 
        {
            productId: {
                type: String,
                required: [true, 'Product Id is required']
            },
            price: {
                type: Number,
                required: [true, 'Price is required']
            },
            quantity: {
                type: Number,
                default: 1
            },
            productPicture: {
                type: String,
                required: [true, 'Product Picture is required']
            },
            productName: {
                type: String,
                required: [true, 'Product Name is required']
            }
        }
    ]
})

module.exports = mongoose.model('Order', orderSchema)