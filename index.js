//dependencies
const cors = require('cors')
const express = require('express')
const mongoose = require('mongoose')
//Allows us to control app's Cross Origin Resource Sharing Settings

//image upload dependencies
const bodyParser = require('body-parser');//imageupload

//Routes
const userRoutes = require('./routes/userRoutes')
const productRoutes = require('./routes/productRoutes')
const orderRoutes = require('./routes/orderRoutes')
const cartRoutes = require('./routes/cartRoutes')

//server setup
const app = express()
const port = 4000

//Allows all resources/origin to access our backend application

app.options('*', cors())
app.use(cors())//Enable all CORS
app.use(express.json({ limit: '50mb' }))
app.use(express.urlencoded({ limit: '50mb', extended: true }))
app.use(bodyParser.json());//bodyparser

//Routes
app.use('/api/users', userRoutes)
app.use('/api/products', productRoutes)
app.use('/api/order', orderRoutes)
app.use('/api/cart', cartRoutes)

//database connection
mongoose.connect("mongodb+srv://admin:Password30x__@cluster0.s5jd2.mongodb.net/capstone2-ecommerce?retryWrites=true&w=majority", {
    useNewUrlParser: true,
    useUnifiedTopology: true
})

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"))
db.once("open", () => console.log("We're connected to the cloud database."))

app.listen(process.env.PORT || port, () => console.log(`Server is running at post ${process.env.PORT || port}`))
//https://enigmatic-inlet-99767.herokuapp.com/ -> heroku

