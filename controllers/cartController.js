const User = require('../models/User')
const Cart = require('../models/Cart')

//Get all cart
module.exports.getAllCart = (reqBody, userData) => {
    return User.findById(userData.userId).then(result => {
        if (result.isAdmin != null) {
            let userResult = result
            return Cart.find({ ownerId: userResult.id }).then(result => {
                return result
            })
        } else {
            return false
        }
    })
}

//Add item to cart
module.exports.addItemCart = (reqBody, userData) => {

    return User.findById(userData.userId).then(result => {
        if (result.isAdmin != true) {
            return Cart.find({productId: reqBody.productId, ownerId: result.id}).limit(1).then((resultCart, error) => {
                if (resultCart.length != 0) {
                    let updatedCart = {
                        quantity: resultCart[0].quantity + parseInt(reqBody.quantity)
                    }
                    return Cart.findByIdAndUpdate(resultCart[0].id, updatedCart).then((cart, error) => {
                        return error ? false : true;
                    })
                } else {
                    let newCart = new Cart({
                        productId: reqBody.productId,
                        quantity: reqBody.quantity,
                        price: reqBody.price,
                        ownerId: result.id,
                        sellerId: reqBody.sellerId,
                        productPicture: reqBody.productPicture,
                        productName: reqBody.productName
                    })

                    return newCart.save().then((cart, error) => {
                        //if create cart failed
                        if (error) {
                            return false;
                        } else {
                            //create cart is successful
                            return true;
                        }
                    })
                }
            })
        } else {
            return false
        }
    });
}

//Edit quantity cart
module.exports.changeQuantity = (reqBody, reqParams, userData) => {
    return User.findById(userData.userId).then(result => {
        if (result.isAdmin != true) {
            if (userData.isAdmin !== null) {

                let updatedCart = {
                    quantity: reqBody.quantity
                }

                return Cart.findByIdAndUpdate(reqParams.cartId, updatedCart).then((cart, error) => {
                    return error ? false : true;
                })

            } else {
                return Promise.reject('Not authorized to access this page');
            }

        } else {
            return false
        }
    });
}

//Remove cart item
module.exports.deleteCartItem = (reqBody, reqParams, userData) => {

    return User.findById(userData.userId).then(result => {
        if (result.isAdmin != true) {
            if (userData.isAdmin !== null) {
                return Cart.findByIdAndDelete(reqParams.cartId).then((cart, error) => {
                    return error ? false : true;
                })
            } else {
                return Promise.reject('Not authorized to access this page');
            }

        } else {
            return false
        }
    });
}
