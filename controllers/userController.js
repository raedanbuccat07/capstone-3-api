const User = require('../models/User.js')
const Cart = require('../models/Cart.js')
const bcrypt = require('bcrypt')
const auth = require('../auth.js')

//cloudinary
const { cloudinary } = require('../utils/cloudinary')

//Get all user
module.exports.getAllUser = (reqBody) => {
    return User.find({}).then(result => {
        return result
    })
}

module.exports.getAllUserPageNumber = (reqParams) => {

    let limitPage = 20
    let skipPage = ((reqParams.pageNumber - 1) * limitPage)

    return User.find({ isAdmin: false }, { password: 0, email: 0 }).skip(skipPage).limit(limitPage).then(result => {
        let oldResult = result
        //return result
        return User.find({ isAdmin: false }).then(result => {
            let totalAllResult = result.length
            oldResult.push({ totalAllResult: totalAllResult })
            return oldResult
        })
    })
}

//Check User Exist
module.exports.checkUser = (reqBody) => {
    return User.findOne({ email: reqBody.email }).then(result => {
        if (result == null) {
            return false
        } else {
            return true
        }
    })
}

module.exports.getProfile = (reqBody, reqParams) => {

    return User.findById(reqParams.userId).then(result => {

        // Changes the value of the user's password to an empty string when returned to the frontend
        // Not doing so will expose the user's password which will also not be needed in other parts of our application
        // Unlike in the "register" method, we do not need to call the mongoose "save" method on the model because we will not be changing the password of the user in the database but only the information that we will be sending back to the frontend application
        result.password = "";

        // Returns the user information with the password as an empty string
        return result;

    })
}

module.exports.getProfileLogin = (data) => {

    return User.findById(data.userId).then(result => {

        // Changes the value of the user's password to an empty string when returned to the frontend
        // Not doing so will expose the user's password which will also not be needed in other parts of our application
        // Unlike in the "register" method, we do not need to call the mongoose "save" method on the model because we will not be changing the password of the user in the database but only the information that we will be sending back to the frontend application
        result.password = "";

        // Returns the user information with the password as an empty string
        return result;

    })

}

/* 
    User registration
    Steps:
        1. Create a new User object using the mongoose model and the info from the request body.
        2. Make sure that the password is encrypted
        3. Save the new user to the database
*/

module.exports.registerUser = async (reqBody) => {

    let profilePictureName = Date.now()

    return await User.findOne({ email: reqBody.email }).then(async result => {
        if (result === null || result === undefined) {
            const fileStr = reqBody.file;
            const uploadResponse = await cloudinary.uploader.upload(fileStr,
                {
                    upload_preset: 'EminaMise',
                    public_id: `profilePictures/${profilePictureName}`
                }
            )
            return uploadResponse
        } else {
            return false
        }
    }).then(async result => {
        if (result !== false) {
            let newUser = new User({
                email: reqBody.email,
                password: bcrypt.hashSync(reqBody.password, 10),
                nickname: reqBody.nickname,
                gender: reqBody.gender,
                profilePicture: result.secure_url,
                profilePictureId: result.public_id
            })

            //save
            return await newUser.save().then((user, error) => {
                //if registration failed
                if (error) {
                    return false;
                } else {
                    //user registration is successful
                    return true
                }
            })
        } else {
            return false
        }
    })
}

//Update user details

module.exports.updateUser = async (reqBody, userData) => {

    let updatedUser = {}

    return await User.findById(userData.id).then(async result => {
        if (result !== null || result !== undefined) {
            const fileStr = reqBody.file;
            const uploadResponse = await cloudinary.uploader.upload(fileStr,
                {
                    public_id: result.profilePictureId
                }
            )
            return uploadResponse
        } else {
            return false
        }
    }).then(async result => {
        if (reqBody.password !== null) {
            let password = reqBody.password
            password = bcrypt.hashSync(reqBody.password, 10)

            updatedUser = {
                email: reqBody.email,
                nickname: reqBody.nickname,
                gender: reqBody.gender,
                password: password,
                profilePicture: result.secure_url,
                profilePictureId: result.public_id
            }
        } else {
            updatedUser = {
                email: reqBody.email,
                nickname: reqBody.nickname,
                gender: reqBody.gender,
                profilePicture: result.secure_url,
                profilePictureId: result.public_id
            }
        }

        return User.findByIdAndUpdate(userData.id, updatedUser).then((user, error) => {
            //if update user failed
            if (error) {
                return false;
            } else {
                //update is successful
                return true;
            }
        })

    })
}

/* 
    Login User
    Steps:
        1. Check if the user email exists in our database. If user does not exist, return false
        2. If the user exists, Compare the password provided in the login form with the password stored in the database
        3. Generate/return a jsonwebtoken if the user is successfully logged in and return false if not
*/

module.exports.loginUser = (reqBody) => {

    return User.findOne({ email: reqBody.email }).then(result => {
        if (result == null) {
            return false
        } else {

            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

            if (isPasswordCorrect) {
                return { accessToken: auth.createAccessToken(result.toObject()) }
            } else {
                return false
            }
        }
    })
}

/* 
    Set User As Admin
    Steps:
        1. An authenticated admin user sends a PUT request containing a JWT in its header to the /:userId/setAsAdmin endpoint.
        2. API validates JWT, returns false if validation fails.
        3. If validation successful, API finds user with ID matching the userId URL parameter and sets its isAdmin property to true.
*/

module.exports.setUserAsAdmin = (reqParams, userData) => {

    return User.findById(userData.id).then(result => {

        if (userData.isAdmin) {

            let newUserType = {
                isAdmin: true
            }

            return User.findByIdAndUpdate(reqParams.userId, newUserType).then((user, error) => {
                if (error) {
                    return false
                } else {

                    let newUserPreview = {
                        email: user.email,
                        isAdmin: true
                    }
                    return true
                }
            })

        } else {
            return Promise.reject('Not authorized to access this page');
        }

    });
}
