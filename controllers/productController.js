const Product = require('../models/Product')
const User = require('../models/User')

//cloudinary
const { cloudinary } = require('../utils/cloudinary')


//Create a new product
/* 
    Steps:
        1. An authenticated admin user sends a POST request containing a JWT in its header to the /products endpoint.
        2. API validates JWT, returns false if validation fails.
        3. If validation successful, API creates product using the contents of the request body.
*/
module.exports.createProduct = async (reqBody, userData) => {

    let productPictureName = Date.now()
    let profilePicture = ""
    let profilePictureId = ""


    return await User.findById(userData.id).then(async result => {
        if (result !== null || result !== undefined) {
            profilePicture = result.profilePicture
            profilePictureId = result.profilePictureId
            const fileStr = reqBody.file;
            const uploadResponse = await cloudinary.uploader.upload(fileStr,
                {
                    upload_preset: 'EminaMise',
                    public_id: `productPictures/${productPictureName}`
                }
            )
            return uploadResponse
        } else {
            return false
        }
    }).then(async result => {
        if (result !== false) {
            let newProduct = new Product({
                sellerId: userData.id,
                name: reqBody.name,
                description: reqBody.description,
                price: reqBody.price,
                productPicture: result.secure_url,
                productPictureId: result.public_id,
                profilePicture: profilePicture,
                profilePictureId: profilePictureId
            })

            return newProduct.save().then((product, error) => {
                //if create product failed
                if (error) {
                    return false;
                } else {
                    //create product is successful
                    return true;
                }
            })
        } else {
            return false
        }
    })
}


//Get all product
module.exports.getAllProduct = (reqBody) => {
    return Product.find({ isActive: true }).then(result => {
        return result
    })
}

//Get all featured product
module.exports.getAllFeaturedProduct = (reqBody) => {
    /*     return Product.find({ isFeatured: true }).then(result => {
            return result
        }) */

    return Product.aggregate(
        [
            {
                $match: {
                    isActive: true,
                    isFeatured: true
                }
            },
            {
                $set: {
                    sellerId: {
                        $toObjectId: "$sellerId"
                    }
                }
            },
            {
                $lookup: {
                    from: "users",
                    localField: "sellerId",
                    foreignField: "_id",
                    as: 'sellerData'
                }
            }
        ]).then(result => {
            return result
        })
}

//Get all product
module.exports.getAllProductAdmin = (reqBody, userData) => {
    return User.findById(userData.id).then((result) => {
        if (result.isAdmin === true) {
            return Product.aggregate(
                [
                    {
                        $set: {
                            sellerId: {
                                $toObjectId: "$sellerId"
                            }
                        }
                    },
                    {
                        $lookup: {
                            from: "users",
                            localField: "sellerId",
                            foreignField: "_id",
                            as: 'userData'
                        }
                    }
                ]).then(result => {
                    return result
                })
        } else {
            return false
        }
    })
}

//Get all product by page number
module.exports.getAllProductPageNumber = (reqBody, reqParams) => {
    //limit(x).skip((pageNo-1)*x)
    let limitPage = 10
    let skipPage = ((reqParams.pageNumber - 1) * limitPage)

    return Product.aggregate(
        [{
            $match: {
                isActive: true
            }
        },
        {
            $lookup: {
                from: "users",
                localField: "profilePictureId",
                foreignField: "profilePictureId",
                as: 'sellerData'
            }
        }
        ]).skip(skipPage)
        .limit(limitPage)
        .then(result => {
            let oldResult = result
            return Product.find({ isActive: true }).then(result => {
                let totalAllResult = result.length
                oldResult.push({ totalAllResult: totalAllResult })
                return oldResult
            })
        });
}

//Get all product by page number
module.exports.getAllOtherProductPageNumber = async (reqBody, userData, reqParams) => {

    return User.findById(userData.id).then((result) => {
        let ownerId = result.id
        //limit(x).skip((pageNo-1)*x)
        let limitPage = 10
        let skipPage = ((reqParams.pageNumber - 1) * limitPage)

        return Product.aggregate(
            [{
                $match: {
                    isActive: true,
                    sellerId: { $ne: ownerId }
                }
            },
            {
                $lookup: {
                    from: "users",
                    localField: "profilePictureId",
                    foreignField: "profilePictureId",
                    as: 'sellerData'
                }
            }
            ]).skip(skipPage)
            .limit(limitPage)
            .then(result => {
                let oldResult = result
                return Product.find({ isActive: true, sellerId: ownerId }).then(result => {
                    let ownerTotalAllResult = result.length
                    return Product.find({ isActive: true }).then(result => {
                        let totalAllResult = result.length - ownerTotalAllResult
                        oldResult.push({ totalAllResult: totalAllResult })
                        return oldResult
                    })
                })
            });
    })
}

/////////////////////////

module.exports.getAllMyProductPageNumber = (reqBody, userData, reqParams) => {
    return User.findById(userData.id).then(result => {
        let ownerId = result.id
        //limit(x).skip((pageNo-1)*x)
        let limitPage = 10
        let skipPage = ((reqParams.pageNumber - 1) * limitPage)
        return Product.find({ isActive: true, sellerId: ownerId }).skip(skipPage).limit(limitPage).then(result => {
            let oldResult = result
            return Product.find({ isActive: true, sellerId: ownerId }).then(result => {
                let totalAllResult = result.length
                oldResult.push({ totalAllResult: totalAllResult })
                return oldResult
            })
        })
    })
}

///////////////////////////////

//Get all product by page number
module.exports.getAllFavoriteProductPageNumber = async (reqBody, userData, reqParams) => {

    return User.findById(userData.id).then((result) => {
        let ownerId = result.id
        //limit(x).skip((pageNo-1)*x)
        let limitPage = 10
        let skipPage = ((reqParams.pageNumber - 1) * limitPage)

        return Product.aggregate(
            [{
                $match: {
                    "isActive": true,
                    "sellerId": { $ne: ownerId },
                    "favorites.userId": ownerId
                }
            },
            {
                $lookup: {
                    from: "users",
                    localField: "profilePictureId",
                    foreignField: "profilePictureId",
                    as: 'sellerData'
                }
            }
            ]).skip(skipPage)
            .limit(limitPage)
            .then(result => {
                let oldResult = result
                let totalAllResult = result.length
                oldResult.push({ totalAllResult: totalAllResult })
                return oldResult
            });
    })
}



///////////////////////////////

module.exports.getAllSellerProductPageNumber = (reqParams) => {

    let sellerId = reqParams.sellerId
    //limit(x).skip((pageNo-1)*x)
    let limitPage = 10
    let skipPage = ((reqParams.pageNumber - 1) * limitPage)
    return Product.find({ isActive: true, sellerId: sellerId }).skip(skipPage).limit(limitPage).then(result => {
        let oldResult = result
        return Product.find({ isActive: true, sellerId: sellerId }).then(result => {
            let totalAllResult = result.length
            oldResult.push({ totalAllResult: totalAllResult })
            return oldResult
        })
    })

}

//Retrieve a single product
module.exports.getProduct = (reqParams) => {
    return Product.findById(reqParams.productId).then(result => {
        return result
    })
}

//Update a product
/* 
    Steps:
        1. An authenticated admin user sends a PUT request containing a JWT in its header to the /products/:productId endpoint.
        2. API validates JWT, returns false if validation fails.
        3. If validation successful, API finds product with ID matching the productId URL 
            parameter and overwrites its info with those from the request body.
*/
module.exports.updateProduct = async (reqBody, reqParams, userData) => {

    let updatedProduct = {}

    return await User.findById(userData.id).then(async result => {

        if (userData.isAdmin !== null) {
            if (reqBody.file !== "" && reqBody.file !== null && reqBody.file !== undefined) {
                if (result !== null || result !== undefined) {
                    const fileStr = reqBody.file;
                    const uploadResponse = await cloudinary.uploader.upload(fileStr,
                        {
                            public_id: reqBody.productPictureId
                        }
                    )

                    updatedProduct = {
                        name: reqBody.name,
                        description: reqBody.description,
                        price: reqBody.price,
                        productPicture: uploadResponse.secure_url,
                    }

                    return true
                } else {
                    return false
                }
            } else {
                updatedProduct = {
                    name: reqBody.name,
                    description: reqBody.description,
                    price: reqBody.price
                }
                return true
            }
        } else {
            return false
        }

    }).then(async result => {
        if (result === true) {
            return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
                if (error) {
                    return false;
                } else {
                    return true;
                }
            })
        }
    })
}

//Remove Product picture

module.exports.deleteProduct = async (reqParams, userData) => {
    return await User.findById(userData.id).then(async result => {
        if (userData.isAdmin !== null) {
            return await Product.findById(reqParams.productId).then(async (product, error) => {

                let productToDelete = product.productPictureId

                if (error) {
                    return false;
                } else {
                    return await Product.findByIdAndDelete(reqParams.productId).then(async (product, error) => {
                        return error ? false : true;
                    }).then(async result => {
                        if (result !== null || result !== undefined) {
                            const uploadResponse = await cloudinary.uploader.destroy(productToDelete)
                            return true
                        } else {
                            return false
                        }
                    })
                }
            })
        } else {
            return false
        }
    })
}

//Archive a product
/* 
    Steps:
        1. An authenticated admin user sends a PUT request containing a JWT in its header to the /products/:productId/archive endpoint.
        2. API validates JWT, returns false if validation fails.
        3. If validation successful, API finds product with ID matching the productId URL parameter and sets its isActive property to false.
*/
module.exports.archiveProduct = (reqBody, reqParams, userData) => {
    return User.findById(userData.id).then(result => {
        if ((userData.isAdmin !== null) || (userData.isAdmin === true)) {

            let updatedProduct = {
                isActive: false,
                isFeatured: false
            }

            return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((course, error) => {
                return error ? false : true
            })

        } else {
            return false
        }
    });
}

//Unarchive a product
/* 
    Steps:
        1. An authenticated admin user sends a PUT request containing a JWT in its header to the /products/:productId/unarchive endpoint.
        2. API validates JWT, returns false if validation fails.
        3. If validation successful, API finds product with ID matching the productId URL parameter and sets its isActive property to true.
*/
module.exports.unarchiveProduct = (reqBody, reqParams, userData) => {

    return User.findById(userData.id).then(result => {

        if ((userData.isAdmin !== null) || (userData.isAdmin === true)) {

            let updatedProduct = {
                isActive: true
            }

            return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((course, error) => {
                return error ? false : true
            })

        } else {
            return false
        }

    });
}

module.exports.featureProduct = (reqBody, reqParams, userData) => {
    return User.findById(userData.id).then(result => {
        if (userData.isAdmin === true) {

            let updatedProduct = {
                isFeatured: true
            }

            return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((course, error) => {
                return error ? false : true
            })

        } else {
            return false
        }
    })
}

//Unarchive a product
/* 
    Steps:
        1. An authenticated admin user sends a PUT request containing a JWT in its header to the /products/:productId/unarchive endpoint.
        2. API validates JWT, returns false if validation fails.
        3. If validation successful, API finds product with ID matching the productId URL parameter and sets its isActive property to true.
*/
module.exports.unfeatureProduct = (reqBody, reqParams, userData) => {

    return User.findById(userData.id).then(result => {

        if (userData.isAdmin === true) {

            let updatedProduct = {
                isFeatured: false
            }

            return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((course, error) => {
                return error ? false : true
            })

        } else {
            return false
        }

    })
}

//Get seller's product 
module.exports.getSellersProducts = (reqBody, reqParams) => {
    return Product.find({ sellerId: reqParams.userId }).then(result => {
        return result
    })
}

//Get (sellers own) MY products
module.exports.getMyProducts = (userData, reqParams) => {
    return User.findById(userData.id).then(result => {

        let ownerId = result.id
        //limit(x).skip((pageNo-1)*x)
        let limitPage = 10
        let skipPage = ((reqParams.pageNumber - 1) * limitPage)
        return Product.find({ sellerId: ownerId }).skip(skipPage).limit(limitPage).then(result => {
            let oldResult = result
            return Product.find({ sellerId: ownerId }).select('_id').then(result => {
                let totalAllResult = result.length
                oldResult.push({ totalAllResult: totalAllResult })
                return oldResult
            })
        })
    })
}


//////////////////////////////////////////////////

module.exports.favoriteProduct = (reqBody, userData) => {

    return User.findById(userData.id).then(result => {
        if (userData.isAdmin !== null) {

            let updateFavorite = []

            updateFavorite.push({
                userId: userData.id
            })

            return Product.findByIdAndUpdate(reqBody.productId, { $push: { favorites: updateFavorite } }).then((user, error) => {
                return true
            })

        } else {
            return false
        }

    });
}

module.exports.unfavoriteProduct = (reqBody, userData) => {

    return User.findById(userData.id).then(result => {
        if (userData.isAdmin !== null) {
            return Product.findByIdAndUpdate(reqBody.productId, { $pull: { favorites: { userId: reqBody.userId } } }).then((user, error) => {
                return true
            })

        } else {
            return false
        }

    });
}

//Get (sellers own) MY products
module.exports.testRoute = () => {
    /*     return Product.aggregate(
            [{
                $match: {
                    sellerId: "61e55fb4599d8a39b8055c54"
                }
            },
            {
    
                $lookup: {
                    from: "users",
                    localField: "profilePicture",
                    foreignField: "profilePicture",
                    as: 'testData'
                }
    
            }
            ]).then(result => {
    
                return result
    
            }); */


    let ownerId = "61e5638b90e409b6ff6b3dbe"
    //limit(x).skip((pageNo-1)*x)
    let limitPage = 10
    let skipPage = ((1 - 1) * limitPage)

    return Product.aggregate(
        [{
            $match: {
                isActive: true,
                sellerId: { $ne: ownerId }
            }
        },
        {

            $lookup: {
                from: "users",
                localField: "profilePictureId",
                foreignField: "profilePictureId",
                as: 'sellerData'
            }

        }
        ]).skip(skipPage)
        .limit(limitPage)
        .then(result => {
            let oldResult = result
            return Product.find({ isActive: true }).then(result => {
                let totalAllResult = result.length
                oldResult.push({ totalAllResult: totalAllResult })
                return oldResult
            })
        });

}

