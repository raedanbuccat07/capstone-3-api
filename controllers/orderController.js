//Main
const Order = require('../models/Order')
//Secondary
const User = require('../models/User')
const Product = require('../models/Product')
const Cart = require('../models/Cart')
const auth = require('../auth')
const { response } = require('express')
const { findById } = require('../models/Product')

/* 
Retrieve All Orders
1. A GET request containing a JWT in its header is sent to the /users/orders endpoint.
2. API validates user is an admin via JWT, returns false if validation fails.
3. If validation is successful, API retrieves all orders and returns them in its response.
*/

module.exports.getAllOrder = (userData) => {

    return User.findById(userData.id).then(result => {
        if (userData.isAdmin) {
            return Order.aggregate(
                [
                    {
                        $set: {
                            userId: {
                                $toObjectId: "$userId"
                            }
                        }
                    },
                    {
                        $lookup: {
                            from: "users",
                            localField: "userId",
                            foreignField: "_id",
                            as: 'buyerData'
                        }
                    }
                ]).then(result => {
                    return result
                })
        } else {
            return false
        }
    })
}

/* 
Create Order
1. An authenticated NON-admin user sends a POST request containing a JWT in its header to the /users/checkout endpoint.
2. API validates user identity via JWT, returns false if validation fails.
3. If validation successful, API creates order using the contents of the request body.
*/

module.exports.createOrder = async (reqBody, userData) => {

    let totalAmount = 0
    let productsPurchased = []

    return User.findById(userData.id).then(result => {

        if (userData.isAdmin) {
            return false
        } else {

            let userResult = result

            return Cart.find({ ownerId: userResult.id }).then(result => {

                for (let i = 0; i < result.length; i++) {
                    let multipliedPrice = result[i].price * result[i].quantity
                    totalAmount = multipliedPrice + totalAmount


                    productsPurchased.push(
                        {
                            productId: result[i].productId,
                            price: result[i].price,
                            quantity: result[i].quantity,
                            productPicture: result[i].productPicture,
                            productName: result[i].productName
                        })
                }

                let newOrder = new Order({
                    totalAmount: parseInt(totalAmount),
                    userId: userResult.id,
                    productsPurchased: productsPurchased
                })

                return newOrder.save().then((order, error) => {
                    if (!error) {
                        return Cart.deleteMany({ ownerId: userResult.id }).then((cart, error) => {
                            return error ? false : true
                        })
                    }
                })
            })
        }

    })
}

/* 
Retrieve Authenticated User’s Orders
1. A GET request containing a JWT in its header is sent to the /users/myOrders endpoint.
2. API validates user is NOT an admin via JWT, returns false if validation fails.
3. If validation is successful, API retrieves orders belonging to authenticated user and returns them in its response.
*/
module.exports.getAllMyOrder = (userData) => {

    return User.findById(userData.id).then(result => {
        if (userData.isAdmin) {
            return Promise.reject('Not authorized to access this page');

        } else {

            return Order.find({ userId: userData.id }).then(result => {
                return result
            })

        }
    })
}
