

//image upload
const { cloudinary } = require('../utils/cloudinary')

module.exports.registerUser = async (reqBody) => {

    let profilePictureName = Date.now()

    return await User.findOne({ email: reqBody.email }).then(async result => {
        if (result === null || result === undefined) {
            const fileStr = reqBody.file;
            const uploadResponse = await cloudinary.uploader.upload(fileStr,
                {
                    upload_preset: 'EminaMise',
                    public_id: `profilePictures/${profilePictureName}`
                }
            )
            return true
        } else {
            return false
        }
    }).then(async result => {
        if(result === true) {
            let newUser = new User({
                email: reqBody.email,
                password: bcrypt.hashSync(reqBody.password, 10),
                nickname: reqBody.nickname,
                gender: reqBody.gender,
                profilePicture: profilePictureName
            })

            //save
            return await newUser.save().then((user, error) => {
                //if registration failed
                if (error) {
                    return false;
                } else {
                    //user registration is successful
                    return true
                }
            })
        }
    })
}