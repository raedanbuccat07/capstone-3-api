const express = require('express')
const router = express.Router()
const orderController = require('../controllers/orderController.js')
const auth = require('../auth.js')

//Routes for getting all orders
router.get('/users/orders', auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)

    orderController.getAllOrder(userData)
    .then(result => res.send(result))
    .catch(error => res.send(error))
})

router.post('/users/checkout', auth.verify, async (req, res) => {

    const userData = auth.decode(req.headers.authorization)

    orderController.createOrder(req.body, userData)
    .then(result => res.send(result))
    .catch(error => res.send(error))
})

//Routes for getting all orders
router.get('/users/myOrders', auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)

    orderController.getAllMyOrder(userData)
    .then(result => res.send(result))
    .catch(error => res.send(error))
})

module.exports = router