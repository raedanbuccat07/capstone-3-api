const express = require('express')
const router = express.Router()
const cartController = require('../controllers/cartController')
const auth = require('../auth')

//Routes for getting all cart
router.get('/', auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization)
    cartController.getAllCart(req.body, { userId: userData.id })
        .then(result => res.send(result))
        .catch(error => res.send(error))
})

//Routes for getting putting cart item
router.post('/addCart', auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization)
    cartController.addItemCart(req.body, { userId: userData.id })
        .then(result => res.send(result))
        .catch(error => res.send(error))
})

//Routes for changing quantity
router.put('/:cartId/changeQuantity', auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)
    cartController.changeQuantity(req.body, req.params, { userId: userData.id })
        .then(result => res.send(result))
        .catch(error => res.send(error))
})

//Routes for changing quantity
router.delete('/:cartId/removeCartItem', auth.verify, (req, res) => {
    
    const userData = auth.decode(req.headers.authorization)
    cartController.deleteCartItem(req.body, req.params, { userId: userData.id })
        .then(result => res.send(result))
        .catch(error => res.send(error))
})

module.exports = router;