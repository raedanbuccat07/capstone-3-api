const express = require('express')
const router = express.Router()
const userController = require('../controllers/userController')
const auth = require('../auth')

//Route for uploading image Sample
/* router.post('/image-upload', upload.single('file'), (req, res) => {

    userController.profilePictureUpload(req.body,req.file).then(result => res.send(result))
}) */

//Routes for getting all user
router.get('/all', (req, res) => {
    userController.getAllUser().then(result => res.send(result))
})

//Routes for getting all user
router.get('/:pageNumber/all', (req, res) => {
    userController.getAllUserPageNumber(req.params).then(result => res.send(result))
})

//Routes for user check
router.post('/checkUser', (req, res) => {
    userController.checkUser(req.body).then(result => res.send(result))
})

//Routes for user registration
router.post('/register', async (req, res) => {
    userController.registerUser(req.body).then(result => res.send(result))
})

//Routes for user update details
router.put('/update', auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)
    userController.updateUser(req.body, userData).then(result => res.send(result))
})

//Routes for authenticating a user
router.post('/login', (req, res) => {
    userController.loginUser(req.body).then(result => res.send(result))
})

//Routes for set user as an admin
router.put('/:userId/setAsAdmin', auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)
    userController.setUserAsAdmin(req.params, userData)
        .then(result => res.send(result))
        .catch(error => res.send(error))
})

//Routes for getting user details
router.get("/details/:userId", (req, res) => {
    userController.getProfile(req.body, req.params).then(resultFromController => res.send(resultFromController));
});

//Routes for getting user details
router.get("/user/details", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)
    userController.getProfileLogin({ userId: userData.id }).then(resultFromController => res.send(resultFromController));
});

module.exports = router;