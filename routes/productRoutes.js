const express = require('express')
const router = express.Router()
const productController = require('../controllers/productController')
const auth = require('../auth')

//POST
//Routes for creating a product
router.post('/', auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)

    productController.createProduct(req.body, userData)
    .then(result => res.send(result))
    .catch(error => res.send(error))
})

////////////////

//PUT
//Routes for favorite product
router.put('/favorite', auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization)
    productController.favoriteProduct(req.body, userData).then(result => res.send(result))
})

//Routes for favorite product
router.put('/unfavorite', auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization)
    productController.unfavoriteProduct(req.body, userData).then(result => res.send(result))
})

//Routes for updating a product
router.put('/:productId', auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)

    productController.updateProduct(req.body, req.params, userData)
    .then(result => res.send(result))
    .catch(error => res.send(error))
})

//Routes for archiving a product
router.put('/:productId/archive', auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)

    productController.archiveProduct(req.body, req.params, userData)
    .then(result => res.send(result))
    .catch(error => res.send(error))
})

//Routes for unarchiving a product
router.put('/:productId/unarchive', auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)
    
    productController.unarchiveProduct(req.body, req.params, userData)
    .then(result => res.send(result))
    .catch(error => res.send(error))
})

//Routes for featuring a product
router.put('/:productId/feature', auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)

    productController.featureProduct(req.body, req.params, userData)
    .then(result => res.send(result))
    .catch(error => res.send(error))
})

//Routes for unfeaturing a product
router.put('/:productId/unfeature', auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)
    
    productController.unfeatureProduct(req.body, req.params, userData)
    .then(result => res.send(result))
    .catch(error => res.send(error))
})


/////////////////////////////////////////////


//GET
//Routes for getting all active product
router.get('/', (req, res) => {
    productController.getAllProduct().then(result => res.send(result))
})

//Routes for getting all featured product
router.get('/featured', (req, res) => {
    productController.getAllFeaturedProduct().then(result => res.send(result))
})


//Routes for getting all active product
router.get('/admin', auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization)
    productController.getAllProductAdmin(req.body, userData).then(result => res.send(result))
})

//Routes for getting all active product
router.get('/page/:pageNumber', (req, res) => {
    productController.getAllProductPageNumber(req.body, req.params).then(result => res.send(result))
})

//Routes for getting all active product excluding owner
router.get('/page/other/:pageNumber', auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization)
    productController.getAllOtherProductPageNumber(req.body, userData, req.params).then(result => res.send(result))
})

//Routes for getting all active product excluding owner
router.get('/page/other/:pageNumber/favorite', auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization)
    productController.getAllFavoriteProductPageNumber(req.body, userData, req.params).then(result => res.send(result))
})

//Routes for getting all active product of the selected seller
router.get('/page/seller/:sellerId/:pageNumber', (req, res) => {
    productController.getAllSellerProductPageNumber(req.params).then(result => res.send(result))
})

//Routes for getting all active owned product page number
router.get('/page/other/own/:pageNumber', auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization)
    productController.getAllMyProductPageNumber(req.body, userData, req.params).then(result => res.send(result))
})

//Routes for getting a product
router.get("/:productId", (req, res) => {
    productController.getProduct(req.params).then(result => res.send(result))
})

//Routes for getting a specific seller's products
router.get("/:userId/seller", (req, res) => {
    productController.getSellersProducts(req.body, req.params).then(result => res.send(result))
})

//Routes for getting MY products
router.get('/seller/:pageNumber/MyProducts', auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)

    productController.getMyProducts(userData, req.params)
    .then(result => res.send(result))
    .catch(error => res.send(error))
})

//DELETE
//Routes for removing a product picture
router.delete('/:productId/delete', auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)

    productController.deleteProduct(req.params, userData)
    .then(result => res.send(result))
    .catch(error => res.send(error))
})



/////////////

router.get('/TEST/ROUTES', (req, res) => {
    productController.testRoute().then(result => res.send(result))
})

module.exports = router;